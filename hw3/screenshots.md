# ScreenShots

- Show (via screenshot) the size of both containers on Docker Hub by
clicking on Tags

![small container size](./static/screenshots/hw3smallDockerhubSize.png)
**hw3small**


![large container size](./static/screenshots/hw3largeDockerhubSize.png)
**hw3large**

- Show the console output displaying container image layers being pulled

![small container img being pulled](./static/screenshots/hw3smallPullRun.png)
**hw3small**


![large container img being pulled](./static/screenshots/hw3largePullRun.png)
**hw3large**

- Connect to your web app locally and show it works (wget or browser)

![hw3small run from browser](./static/screenshots/hw3smallBrowser.png)  
**hw3small**


![hw3large run from browser](./static/screenshots/hw3largeBrowser.png)

**hw3large**