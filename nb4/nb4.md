Evan DePosit  
12/14/2019  
CS 530- Cloud and Internet Systems  
Lab Notebook #4

# API
## Lab 1

- Click on the function and show the amount of RAM allocated to it

![ram allocated](./api/ram_allocated.png)


- Bring up the "Trigger" tab.  Click on it and show the results returning all entries as a JSON object

![json object from trigger tab](./api/click_on_trigger.png)

- Bring up the "Testing" tab, Specify a JSON payload with the appropriate fields and a message "API Lab #1"
- Click on "Test the Function", then show the results returned in the "Output" field.

![test func](./api/test_func.png)

- Show the response status (resp.status_code)
- Show the response headers (resp.headers) and show the data
type of its response headers (via type())?
- Show the response text (resp.text) and show the data type of it

![resp status through response text](./api/resp_status_to_text.png)

- Show the response parsed as json (resp.json()) and its type()

![resp json](./api/resp_json.png)

- Assign resp.json() to a variable
- Use the interpreter to individually print the name, email, signed_on, and message of the first Guestbook entry returned

![print response](./api/indiv_print.png)

- Submit a POST request onto the endpoint with the dictionary
- Show the response status, the response headers, and the response text
that indicate a successful insertion
![post response](./api/post.png)

# ML APIs Labs
## Lab 1
- Run a detection that returns the labels generated with an image of a bird
given its URI, show the output

![bird detection](./ml/lab1/detectionBird.png)

- Look for code that allows you to run the script to detect logos in images
given a URI. Then, use the script to run a detection on a logo you find on the Internet

![logo detection](./ml/lab1/detectionLogo.png)

- Run transcribe.py on the given URI and show output

![transcribe given URI](./ml/lab1/transcribeGivenURI.png)

- Run snippets.py on the text string and show output

![translate characters](./ml/lab1/translateCharacters.png)

- Run the entities-text function in snippets.py and show output

![entities text function](./ml/lab1/entitiesTextFunc.png)

- Edit the string used in sentiment_text() and run the script using
the following strings and show how the sentiment score varies each time

![sentiment analysis: hello, world](./ml/lab1/sentHelloWorld.png)
**Hello, world!**

![sentiment analysis: hw is awful](./ml/lab1/sentHWAwfulExcl.png)
**homework is awful!**

![sentiment analysis: hw is awesome?](./ml/lab1/sentHWAwesomeQuest.png)
**homework is awesome?**

![sentiment analysis: hw is awesome.](./ml/lab1/sentHWAwesomePeriod.png)
**homework is awesome.**

![sentiment analysis: hw is awesome!](./ml/lab1/sentHWAwesomeExcl.png)
**homework is awesome!**

- Run at least 3 pairs other than the one given in the walk-through
```
python3 solution.py tr-TR gs://ml-api-codelab/tr-ball.wav \
gs://ml-api-codelab/football.jpg
```
1. bicycle.jpg and de-bike.wav
```
python3 solution.py tr-TR gs://ml-api-codelab/de-bike.wav \
gs://ml-api-codelab/bicycle.jpg
```
![bicycle, bike](./ml/lab1/bike_bicycle.png)

2. bicycle.jpg and tr-ball.wav
```
python3 solution.py tr-TR gs://ml-api-codelab/tr-ball.wav \
gs://ml-api-codelab/bicycle.jpg
```
![bicycle, ball](./ml/lab1/bicycle_ball.png)

3. coat_rack.jpg and de-jacket.wav
```
python3 solution.py tr-TR gs://ml-api-codelab/de-jacket.wav \
gs://ml-api-codelab/coat_rack.jpg
```
![bicycle, ball](./ml/lab1/coatrack_jacket.png)


## Lab 2 
- Show full image of the Otter Crossing sign via your bucket

![otter crossing in bucket](./ml/lab2/otterCrossing_bucket.png)

- Then use Vision's text_detection() to perform an OCR
operation on a picture of the sign

![otter crossing text detection](./ml/lab2/otterCrossing_detect.png)

- Show full Eiffel Tower image in your bucket

![eiffel tower in bucket](./ml/lab2/eiffelTower_bucket.png)

- Then use Vision's landmark_detection() to identify it of famous places

![eiffel tower landmark detection](./ml/lab2/eiffel_landmarks_uri.png)

- Show the two face images in your bucket

![no surprise face in bucket](./ml/lab2/face_noSurprise_bucket.png)

![surprise face in bucket](./ml/lab2/face_surprise_bucket.png)

- Then use Vision's face_detection() to annotate images

![no surprise faces-uri](./ml/lab2/noSurprise_faces_uri.png)

![surprise faces-uri](./ml/lab2/surprise_faces_uri.png)

## Lab 3 
- Copy the video to a storage bucket

![blooper video in bucket](./ml/lab3/bloopers_bucket.png)

- Run the code to perform the analysis

![blooper video analysis](./ml/lab3/bloopers_detect.png)

1. Which sports did the API properly identify?

basketball, hockey, and baseball (ball game)

2. Which sports did the API fail to identify?

football and soccer

- Upload a short (< 2 min) video of your own to a Cloud Storage bucket and run the label script on it

![skateboard bucket](./ml/lab3/skateboard_bucket.png)

3. Show an example of a properly identified entity in your video

Label description: outdoor recreation, kickflip, skateboarding, ollie, footwear, boardsport, skateboard equipment and supplies, skateboarding trick, sports, skateboarder

![skateboard labels](./ml/lab3/skateboard_label.png)

4. Show an example of a missed or misclassified entity in your video

![skateboard mis-labels](./ml/lab3/misclassify.png)

## Lab 4
24 mins

# AutoML 
## Lab 1 

# Firebase Labs
## Lab 1

# Data Labs
## Cloud Dataproc Lab 1
- View cluster on Compute Engine

![cluster on compute engine](./dataproc/lab1/cluster_on_compute_engine.png)

1. When done, note the time. How long did it take?
About a minute

- Show the estimate for pi

![pi estimate](./dataproc/lab1/pi_is.png)

- Allocate two pre-emptible machines to the cluster. Show them in Compute Engine

![pre-emptible machines in compute engine](./dataproc/lab1/two_premptible.png)

2. When done, note the time. How long did it take?
less than a minute

- Show the estimate for pi

![second pi estimate](./dataproc/lab1/pi_is_2.png)

- ssh into the master node.  Once logged in, get the hostname. List the cluster to show all VMs

![ssh into master](./dataproc/lab1/ssh.png)


- Upon completion, click on job, then click on Line wrapping to see
output

![ui pi estimate](./dataproc/lab1/pi_is_ui.png)
**Web UI pi estimate**

## Cloud Dataproc Lab 2

- Go to Dataproc -> Clusters. Show the Jobs detail, the VM instances
created, and the configuration

![job detail](./dataproc/lab2/job_detail.png)
**Jobs Details**

![vm instances](./dataproc/lab2/instances_created.png)
**VM Instances**

![configuration](./dataproc/lab2/configuration.png)
**Configuration**

- Go to Dataproc -> Jobs and show the job output and configuration
via screenshot

![job output](./dataproc/lab2/job_output.png)
**Job Output**

![job config](./dataproc/lab2/job_config.png)
**Job Configuration**

- Download and screenshot the processed images in the out/ folder. Go to the Cloud Vision page, scroll down to "Try the API", download one of the 3 original images used, and use the Vision API to perform the same operation. 

1. Compare the results

the Dataproc version identified an extra face on the Africans Woman's shoulder.  It also did not identify the student's face in the second row who is learning to the right.  On the Family picture Dataproc only drew the rectangle around the man's face and identified another face on the woman's chest.  The Google Vision API was able to identify more faces and draw better boundaries around them.

![Try the API: African Woman ](./dataproc/lab2/try_api_aff.png)
**Try the API: African Woman**

![processed images from out/: African Woman](./dataproc/lab2/out_african_women.png)
**processed images from out/: African Woman**

![Try the API: Class](./dataproc/lab2/try_api_class.png)
**Try the API: Classroom**

![processed images from out/: class](./dataproc/lab2/out_classroom.png)
**processed images from out/: Classroom**

![Try the API: Family](./dataproc/lab2/try_api_fam.png)
**Try the API: Family**

![processed images from out/: family](./dataproc/lab2/out_family.png)
**processed images from out/: Family-of-Three**

## Cloud Dataflow Lab 1

![local output](./dataflow/view_output_locally.png)

![line count](./dataflow/line_count_local_output.png)

## BigQuery Lab 1

- Once you see your table has been created, click on it and go to Preview, show the number of rows in Details

![number of rows](./bigquery/lab1/num_of_rows.png)

![Query Method 1: Query Editor](./bigquery/lab1/query_editor.png)

**Query Method 1: Query Editor**

![Query Method 2: command line](./bigquery/lab1/cl_query.png)

**Query Method 2: Command Line**

![Query Method 3: big query sh](./bigquery/lab1/bq_shell.png)

**Query Method 3: BigQuery Shell**

- Is your name in the dataset for 2014? How popular was it?
Screenshot your results.

![your name query](./bigquery/lab1/name.png)



## BigQuery Lab 2
- Click on the tables and then click Preview to find the number of blocks
that are currently being stored on a full node.

![number of blocks](./bigquery/lab2/num_blocks.png)

![number of blocks](./bigquery/lab2/num_blocks_rows.png)

- Click on Details to find the size of the block-chain in BigQuery
(uncompressed). Screenshot both results.

![size of blockchain](./bigquery/lab2/details_size_block-chain.png)

- Visit dataset containing all github commits. Click on Preview and examine the columns associated with commits 

![commit preview](./bigquery/lab2/commits_preview.png)

- Click on Details to find the size of the commits table

![commit details](./bigquery/lab2/commit_details.png)

- Enter a query to find commits with duplicate subject lines (commit messages)

![duplicate commit query](./bigquery/lab2/duplicate_commit_msg.png)

1. What is the most common subject message used? Answer this in
your lab notebook.

Update README.md

- Run query to find projects with the most contributors

![most contributors](./bigquery/lab2/top_num_contributors.png)

2. What project is has the top number of contributors? Screenshot your
results. Extract name of repo from repo_name path.

dotfiles

- Run query to find most popular languages used in pull requests.
What language is this? Screenshot.

![most popular lang](./bigquery/lab2/most_pop_lang.png)

## BigQuery Lab 3

![error](./bigquery/lab3/bq_lab3_error.png)

## Cloud Datalab Lab 1

## Cloud Datalab Lab 2 