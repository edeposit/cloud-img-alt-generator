clusterName: evandeposit-dplab
clusterUuid: 2f625f5e-63a0-40b8-b1ec-de8b59462773
config:
  configBucket: dataproc-b6f9caf3-1604-437f-893e-4fe5b0edce1d-us
  gceClusterConfig:
    networkUri: https://www.googleapis.com/compute/v1/projects/expanded-augury-254716/global/networks/default
    serviceAccountScopes:
    - https://www.googleapis.com/auth/cloud-platform
    - https://www.googleapis.com/auth/cloud.useraccounts.readonly
    - https://www.googleapis.com/auth/devstorage.read_write
    - https://www.googleapis.com/auth/logging.write
    tags:
    - codelab
    zoneUri: https://www.googleapis.com/compute/v1/projects/expanded-augury-254716/zones/us-west1-b
  masterConfig:
    diskConfig:
      bootDiskSizeGb: 500
      bootDiskType: pd-standard
    imageUri: https://www.googleapis.com/compute/v1/projects/cloud-dataproc/global/images/dataproc-1-3-deb9-20191111-000000-rc01
    instanceNames:
    - evandeposit-dplab-m
    machineTypeUri: https://www.googleapis.com/compute/v1/projects/expanded-augury-254716/zones/us-west1-b/machineTypes/n1-standard-4
    minCpuPlatform: AUTOMATIC
    numInstances: 1
  softwareConfig:
    imageVersion: 1.3.46-debian9
    properties:
      capacity-scheduler:yarn.scheduler.capacity.root.default.ordering-policy: fair
      core:fs.gs.block.size: '134217728'
      core:fs.gs.metadata.cache.enable: 'false'
      core:hadoop.ssl.enabled.protocols: TLSv1,TLSv1.1,TLSv1.2
      distcp:mapreduce.map.java.opts: -Xmx768m
      distcp:mapreduce.map.memory.mb: '1024'
      distcp:mapreduce.reduce.java.opts: -Xmx768m
      distcp:mapreduce.reduce.memory.mb: '1024'
      hdfs:dfs.datanode.address: 0.0.0.0:9866
      hdfs:dfs.datanode.http.address: 0.0.0.0:9864
      hdfs:dfs.datanode.https.address: 0.0.0.0:9865
      hdfs:dfs.datanode.ipc.address: 0.0.0.0:9867
      hdfs:dfs.namenode.handler.count: '20'
      hdfs:dfs.namenode.http-address: 0.0.0.0:9870
      hdfs:dfs.namenode.https-address: 0.0.0.0:9871
      hdfs:dfs.namenode.lifeline.rpc-address: evandeposit-dplab-m:8050
      hdfs:dfs.namenode.secondary.http-address: 0.0.0.0:9868
      hdfs:dfs.namenode.secondary.https-address: 0.0.0.0:9869
      hdfs:dfs.namenode.service.handler.count: '10'
      hdfs:dfs.namenode.servicerpc-address: evandeposit-dplab-m:8051
      mapred-env:HADOOP_JOB_HISTORYSERVER_HEAPSIZE: '3840'
      mapred:mapreduce.job.maps: '21'
      mapred:mapreduce.job.reduce.slowstart.completedmaps: '0.95'
      mapred:mapreduce.job.reduces: '7'
      mapred:mapreduce.map.cpu.vcores: '1'
      mapred:mapreduce.map.java.opts: -Xmx2457m
      mapred:mapreduce.map.memory.mb: '3072'
      mapred:mapreduce.reduce.cpu.vcores: '1'
      mapred:mapreduce.reduce.java.opts: -Xmx2457m
      mapred:mapreduce.reduce.memory.mb: '3072'
      mapred:mapreduce.task.io.sort.mb: '256'
      mapred:yarn.app.mapreduce.am.command-opts: -Xmx2457m
      mapred:yarn.app.mapreduce.am.resource.cpu-vcores: '1'
      mapred:yarn.app.mapreduce.am.resource.mb: '3072'
      spark-env:SPARK_DAEMON_MEMORY: 3840m
      spark:spark.driver.maxResultSize: 1920m
      spark:spark.driver.memory: 3840m
      spark:spark.executor.cores: '2'
      spark:spark.executor.instances: '2'
      spark:spark.executor.memory: 5586m
      spark:spark.executorEnv.OPENBLAS_NUM_THREADS: '1'
      spark:spark.scheduler.mode: FAIR
      spark:spark.sql.cbo.enabled: 'true'
      spark:spark.yarn.am.memory: 640m
      yarn-env:YARN_HEAPSIZE: '3840'
      yarn-env:YARN_TIMELINESERVER_HEAPSIZE: '3840'
      yarn:yarn.nodemanager.resource.memory-mb: '12288'
      yarn:yarn.resourcemanager.nodemanager-graceful-decommission-timeout-secs: '86400'
      yarn:yarn.scheduler.maximum-allocation-mb: '12288'
      yarn:yarn.scheduler.minimum-allocation-mb: '1024'
  workerConfig:
    diskConfig:
      bootDiskSizeGb: 500
      bootDiskType: pd-standard
    imageUri: https://www.googleapis.com/compute/v1/projects/cloud-dataproc/global/images/dataproc-1-3-deb9-20191111-000000-rc01
    instanceNames:
    - evandeposit-dplab-w-0
    - evandeposit-dplab-w-1
    machineTypeUri: https://www.googleapis.com/compute/v1/projects/expanded-augury-254716/zones/us-west1-b/machineTypes/n1-standard-4
    minCpuPlatform: AUTOMATIC
    numInstances: 2
labels:
  goog-dataproc-cluster-name: evandeposit-dplab
  goog-dataproc-cluster-uuid: 2f625f5e-63a0-40b8-b1ec-de8b59462773
  goog-dataproc-location: global
metrics:
  hdfsMetrics:
    dfs-blocks-corrupt: '0'
    dfs-blocks-missing: '0'
    dfs-blocks-missing-repl-one: '0'
    dfs-blocks-pending-deletion: '0'
    dfs-blocks-under-replication: '0'
    dfs-capacity-present: '1003998932992'
    dfs-capacity-remaining: '1003985772544'
    dfs-capacity-total: '1056751181824'
    dfs-capacity-used: '13160448'
    dfs-nodes-decommissioned: '0'
    dfs-nodes-decommissioning: '0'
    dfs-nodes-running: '2'
  yarnMetrics:
    yarn-apps-completed: '3'
    yarn-apps-failed: '0'
    yarn-apps-killed: '0'
    yarn-apps-pending: '0'
    yarn-apps-running: '0'
    yarn-apps-submitted: '3'
    yarn-containers-allocated: '0'
    yarn-containers-pending: '0'
    yarn-containers-reserved: '0'
    yarn-memory-mb-allocated: '0'
    yarn-memory-mb-available: '24576'
    yarn-memory-mb-pending: '0'
    yarn-memory-mb-reserved: '0'
    yarn-memory-mb-total: '24576'
    yarn-nodes-active: '2'
    yarn-nodes-decommissioned: '0'
    yarn-nodes-lost: '0'
    yarn-nodes-rebooted: '0'
    yarn-nodes-unhealthy: '0'
    yarn-vcores-allocated: '0'
    yarn-vcores-available: '8'
    yarn-vcores-pending: '0'
    yarn-vcores-reserved: '0'
    yarn-vcores-total: '8'
projectId: expanded-augury-254716
status:
  state: RUNNING
  stateStartTime: '2019-12-13T00:16:35.406Z'
statusHistory:
- state: CREATING
  stateStartTime: '2019-12-13T00:15:13.329Z'
