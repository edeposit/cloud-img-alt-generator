WARNING: Dataproc --region flag will become required in January 2020. Please either specify this flag, or set default by running 'gcloud config set dataproc/region <your-default-region>'
WARNING: Dataproc --region flag will become required in January 2020. Please either specify this flag, or set default by running 'gcloud config set dataproc/region <your-default-region>'
Job [dfc5e9fac8214d18afee2efb3c152a2e] submitted.
Waiting for job output...
19/12/13 00:24:09 INFO org.spark_project.jetty.util.log: Logging initialized @3125ms
19/12/13 00:24:09 INFO org.spark_project.jetty.server.Server: jetty-9.3.z-SNAPSHOT, build timestamp: unknown, git hash: unknown
19/12/13 00:24:09 INFO org.spark_project.jetty.server.Server: Started @3254ms
19/12/13 00:24:09 INFO org.spark_project.jetty.server.AbstractConnector: Started ServerConnector@56303475{HTTP/1.1,[http/1.1]}{0.0.0.0:4040}
19/12/13 00:24:09 WARN org.apache.spark.scheduler.FairSchedulableBuilder: Fair Scheduler configuration file not found so jobs will be scheduled in FIFO order. To use fair scheduling, configure pools in fairscheduler.xml or set spark.scheduler.allocation.file to a file that contains the configuration.
19/12/13 00:24:10 INFO org.apache.hadoop.yarn.client.RMProxy: Connecting to ResourceManager at evandeposit-dplab-m/10.138.0.31:8032
19/12/13 00:24:10 INFO org.apache.hadoop.yarn.client.AHSProxy: Connecting to Application History server at evandeposit-dplab-m/10.138.0.31:10200
19/12/13 00:24:13 INFO org.apache.hadoop.yarn.client.api.impl.YarnClientImpl: Submitted application application_1576196153801_0001
Pi is roughly 3.1416807514168075
19/12/13 00:24:33 INFO org.spark_project.jetty.server.AbstractConnector: Stopped Spark@56303475{HTTP/1.1,[http/1.1]}{0.0.0.0:4040}
Job [dfc5e9fac8214d18afee2efb3c152a2e] finished successfully.
driverControlFilesUri: gs://dataproc-b6f9caf3-1604-437f-893e-4fe5b0edce1d-us/google-cloud-dataproc-metainfo/2f625f5e-63a0-40b8-b1ec-de8b59462773/jobs/dfc5e9fac8214d18afee2efb3c152a2e/
driverOutputResourceUri: gs://dataproc-b6f9caf3-1604-437f-893e-4fe5b0edce1d-us/google-cloud-dataproc-metainfo/2f625f5e-63a0-40b8-b1ec-de8b59462773/jobs/dfc5e9fac8214d18afee2efb3c152a2e/driveroutput
jobUuid: a1e48bde-9bc4-310d-ac3c-d5c126ed4efc
placement:
  clusterName: evandeposit-dplab
  clusterUuid: 2f625f5e-63a0-40b8-b1ec-de8b59462773
reference:
  jobId: dfc5e9fac8214d18afee2efb3c152a2e
  projectId: expanded-augury-254716
sparkJob:
  args:
  - '1000'
  jarFileUris:
  - file:///usr/lib/spark/examples/jars/spark-examples.jar
  mainClass: org.apache.spark.examples.SparkPi
status:
  state: DONE
  stateStartTime: '2019-12-13T00:24:35.379Z'
statusHistory:
- state: PENDING
  stateStartTime: '2019-12-13T00:24:03.199Z'
- state: SETUP_DONE
  stateStartTime: '2019-12-13T00:24:03.621Z'
- details: Agent reported job success
  state: RUNNING
  stateStartTime: '2019-12-13T00:24:04.311Z'
yarnApplications:
- name: Spark Pi
  progress: 1.0
  state: FINISHED
  trackingUrl: http://evandeposit-dplab-m:8088/proxy/application_1576196153801_0001/
