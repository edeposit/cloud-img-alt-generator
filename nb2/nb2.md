Evan DePosit  
11/3/2019  
CS 530- Cloud and Internet Systems  
Lab Notebook #2

# WebDev  
## Lab 1  
- Bring up a browser on the VM and point the browser to the web app (localhost:5000).  Add several entries that include your OdinID and show the resulting
page.   
![lab1 picture](./webdev/lab1.png)    

- Run the install script, show site.   
![lab1 picture](./webdev/webdev_browser.png)  

# Databases  
## Lab 1   
- Screenshot of "Check your answers" . 
![database quiz](./databases/lab1/databases_lab1.png)  

## Lab 2   
- View tables created.  
![show table](./databases/lab2/databases_lab2_showTable.png)  

- Verify data.     
![select star](./databases/lab2/databases_lab2_selectStarFromRating.png)  

- Run queries for accommodations at various price levels and types.   

![query 1](./databases/lab2/lab2_likeQuery.png)   
**Query 1** 

![query 2](./databases/lab2/lab2_roomQuery.png) 
**Query 2** 

![query 3](./databases/lab2/lab2_joinQuery.png)  
**Query 3** 

## Lab 3

![aws instance](./databases/lab3/aws_instance.png)

![aws sql](./databases/lab3/aws_sql.png)

# Containers
## lab 1  
- Show the image generated and list the size of the image created  
![container images](./containers/lab1/show_images.png)  


- Show the container in the docker listing to find its name hellou under the "NAMES" column  
![containers in the docker listing](./containers/lab1/dockerps.png)   


- Do an ls and a ps to show the filesystem and processes running in the container  
![ps to show processes running in container](./containers/lab1/interactiveShell.png)   


- Run the image directly from Docker Hub.  Show the output of the command  
![run image from Docker hub](./containers/lab1/outputRunImgDockerHub.png)   


- Validate that the container works by retrieving `http://127.0.0.1:8000/` user wget  
![wget](./containers/lab1/wget.png)   
 

- Show the container image on Docker Hub  
![container img on docker hub](./containers/lab1/showContainerImgDockerHub.png)   


- Show the container image metadata on microBadger  
![microbadger](./containers/lab1/showMicroBadger.png)   


## lab 2   
- Show the image generated and list the size of the image created  
![container images](./containers/lab2/imgLis.png)  


- Show the container in the docker listing to find its name hellou under the "NAMES" column  
![containers in the docker listing](./containers/lab2/showContainerPs.png)   

- for the docker exe command, what is the output?
![output of docker exec](./containers/lab2/exec.png)

1. What happens when you replace `/bin/bash` with `/bin/sh` in the `docker exec` command?    
It causes it to work.  
 

- Do an ls and a ps to show the filesystem and processes running in the container  
![ps to show processes running in container](./containers/lab2/filesystemsAndProcessesRunning.png)   


- Run the image directly from Docker Hub.  Show the output of the command  
![run image from Docker hub](./containers/lab2/ouputOfRunFromDockerhub.png )   


- Validate that the container works by retrieving `http://127.0.0.1:8000/` user wget  
![wget](./containers/lab2/runWithWget.png)   
 

- Show the container image on Docker Hub  
![container img on docker hub](./containers/lab2/containerOnDH.png)   


- Show the container image metadata on microBadger  
![microbadger](./containers/lab2/microBadger.png)   


## lab 3
![lab3IP.png](./containers/lab3/lab3IP.png)  


![lab3browser.png](./containers/lab3/lab3browser.png)   

## lab 4
![lab4IP.png](./containers/lab4/lab4IP.png)  


![lab4browser.png](./containers/lab4/lab4browser.png)   