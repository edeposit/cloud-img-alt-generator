"""
A simple bubble tea flask app for findind bubble tea stores and adding new entries to list of bubble tea shops.
"""
from flask import Flask, redirect, request, url_for, render_template

from model_datastore import model
#from model_sqlite3 import model
#from model_pylist import model

app = Flask(__name__)       # our Flask app
model = model()

"""
Function decorator === app.route('/',index())
"""
@app.route('/')
@app.route('/index.html')
def index():
    """
    accepts requests for root or index.html, and returns landing page that contains stock image for bubble tea with navbar on top
    """
    return render_template('index.html')

@app.route('/add', methods=['GET'])
def add():
    """
    Accepts GET requests, and processes the form to add new bubble tea shops to model;
    Redirect to new when completed.
    returns add page with fillable form
    """
    return render_template('add.html')

@app.route('/new', methods=['POST'])
def new():
    """
    Accepts POST requests to add new bubble tea shops to model;
    Redirect to index when completed.
    """
    model.insert(name=request.form['name'], address=request.form['address'], city=request.form['city'], 
    state=request.form['state'], zipCode=request.form['zip'], hours=request.form['hours'], 
    phone=request.form['phone'], rating=request.form['rating'], review=request.form['review'])

    return redirect(url_for('index'))
    
@app.route('/list', methods=['GET'])
def list():
    """
    Accepts GET requests, and returns page with list of bubble tea shops from model;
    """
    entries= model.select() 

    #create list of diconaries so values can be accessed by key for each shop
    shopData=[]
    for shop in entries:
        a, b, c, d, e, f, g, h, i=shop
        shopDict= {'name':a, 'address':b, 'city':c, 'state':d, 'zipCode':e, 'hours':f, 'phone':g, 'rating':h, 'review':i}
        shopData.append(shopDict)

    return render_template('list.html', entries=shopData)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=8000)
