import sqlite3
from Model import Model
DB_FILE = 'entries.db'


#how does this authenticate with google cloud?  Might have to run from cloud shell
class model(Model):
    def __init__(self):
        connection = sqlite3.connect(DB_FILE)
        cursor =  connection.cursor()
        try:
            cursor.execute("select count(rowid) from allShops")
        except sqlite3.OperationalError:
            cursor.execute('''create table allShops (name text, address text, city text, state text,
            zipCode text, hours text, phone text, rating text, review text)''')
            
        cursor.close

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, address, city, state, zip code, hours, phone, rating, and review
        :return: List of tuples
        """
        connection = sqlite3.connect(DB_FILE)
        cursor =  connection.cursor()

        cursor.execute("SELECT * FROM allShops")
        shopList= cursor.fetchall()
        
        connection.commit()
        cursor.close()
        return shopList 

        
    def insert(self, name=None, address=None, city=None,
    state=None, zipCode=None, hours=None, phone=None,
    rating=None, review=None):
        """
        inserts a new bubble tea shop  entry into table allShops
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zipCode: String
        :param hours: String
        :param phone: String
        :param rating: String
        :param review: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        #create dictionary from form data
        params = {'name':name, 'address':address, 'city':city, 'state':state, 'zipCode':zipCode, \
            'hours':hours, 'phone':phone, 'rating':rating, 'review':review}
        
        connection = sqlite3.connect(DB_FILE)
        cursor =  connection.cursor()

        cursor.execute('''insert into allShops (name, address, city, state, zipCode, hours, phone, rating, review)
        VALUES (:name, :address, :city, :state, :zipCode, :hours, :phone, :rating, :review)''', params)
        
        connection.commit()
        cursor.close()
        return True

