"""
Data is stored in a Python list.  Returns a list of lists
  upon retrieval
"""
from datetime import date
from Model import Model

class model(Model):
    def __init__(self):
        """
        initializes list of bubble tea shops
        """
        self.allShops=[]

    def select(self):
        """
        Gets all rows from the database as list of lists
        Each row in allShops contains: name, address, city, state, zip code, hours, phone, rating, and revierw
        :return: List of lists
        """
        return self.allShops

    def insert(self, name=None, address=None, city=None,
        state=None, zipCode=None, hours=None, phone=None,
        rating=None, review=None):
        """
        Appends a new list of values representing information for a bubble tea store into allShops
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zipCode: String
        :param hours: String
        :param phone: String
        :param rating: String
        :param review: String
        :return: True
        """
        newShop=[]
        newShop.append(name)
        newShop.append(address)
        newShop.append(city)
        newShop.append(state)
        newShop.append(zipCode)
        newShop.append(hours)
        newShop.append(phone)
        newShop.append(rating)
        newShop.append(review)

        self.allShops.append(newShop)
        
        return True

