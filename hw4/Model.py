class Model():
    def select(self):
        """
        Gets all bubble shop entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, address, city, state, 
    zipCode, hours, phone, rating, review):
        """
        inserts a new bubble tea shop  entry into table allShops
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zipCode: String
        :param hours: String
        :param phone: String
        :param rating: String
        :param review: String
        :return: none
        """
        pass
