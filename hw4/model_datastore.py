# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from Model import Model
from datetime import datetime
from google.cloud import datastore

#where does this function get called?
def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.

    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]

    This returns:
        [ name, address, city, state, zipCode, hours, phone, rating, review ]
    where name, address, city, state, zipCode, hours, phone, rating, and  review are Python strings
    """
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()

    #makes back into a list so that it can be turned back into dictionary in app.py
    return [entity['name'] ,entity['address'], entity['city'], entity['state'], entity['zipCode'], entity['hours'], entity['phone'], entity['rating'], entity['review']]

class model(Model):
    def __init__(self):
        self.client = datastore.Client('expanded-augury-254716')
    
    def select(self):
        """
        Gets all entries from the datastore
        Each entry contains: name, address, city, state, zip code, hours, phone, rating, and review
        :return: List of tuples
        """
        query = self.client.query(kind = 'bubbleTeaShop')
        entities = list(map(from_datastore,query.fetch()))
        return entities

    def insert(sel        """
        inserts a new bubble tea shop  entry into table allShops
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zipCode: String
        :param hours: String
        :param phone: String
        :param rating: String
        :param review: String
        :return: True
        """
 f, name=None, address=None, city=None, state=None, zipCode=None,
        hours=None, phone=None, rating=None, review=None):

        key = self.client.key('bubbleTeaShop')
        rev = datastore.Entity(key)
        rev.update( {
            'name':name,
            'address':address,
            'city':city,
            'state':state,
            'zipCode':zipCode,
            'hours':hours,
            'phone':phone,
            'rating':rating,
            'review':review
            })
        self.client.put(rev)
        return True
