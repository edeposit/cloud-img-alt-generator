Evan DePosit  
10/20/2019  
CS 530- Cloud and Internet Systems  
Lab Notebook  

# ARP

## Lab 1

- Use the ifconfig command to find the IP address and hardware
address of the local virtual ethernet card interface.  

![ifconfig command](./arp/ifconfig.png)  
**The IP address shown above confirms the following wireshark captures also came from my machine**  

- Perform a netstat –rn to find default router's IP address 

![netstat command](./arp/netstat.png)

- Ping the default router and use arp to find its hardware address.  

![netstat command](./arp/arp.png)

- Then, in a separate terminal, ping www.google.com.  

![request packet](./arp/wireshark_request.png)
**Above: Request Packet**  

1. who owns the destination hardware address of the packet (request)?  
*It is the mac address of the default gateway router as shown from the arp command.*   

![response packet](./arp/wireshark_response.png)
**Above: Response Packet**  


2. Who owns the destination hardware address of the packet (response)?  
*It is the eathernet mac address of the virtual box as shown from the ifconfigt command.*  


# Network  

## lab 1: netsim  

- Show screenshot of completed list of levels  

![netism 1](./network/lab1/netism1.png)  
  
![netism 2](./network/lab1/netism2.png)  
  
![netism 3](./network/lab1/netism3.png)  

- For Level #5, Show packet before it hits modem and after it leaves modem    

![before hitting modem](./network/lab1/before_modem.png)    
**Above: Packet before it hits the modem.**  

![after hitting modem](./network/lab1/after_modem.png)    
**Above: Packet after it hits the modem.**   

## lab 2: nmap

![nmap output](./network/lab2_3/network_lab_2.png)

## lab 3: Subnets in the cloud

![nmap output](./network/lab2_3/network_lab_3.png)  

# Transport

## Lab 1: netstat, nc

![vm netstat](./transport/lab1/lab1_vm_netstat.png)

![ada netstat](./transport/lab1/lab1_ada_nestat.png)

![version](./transport/lab1/transport_lab1_ssh_version.png)

## Lab 2: iperf and TCP Performance

![US Throughput](./transport/lab2/transport_lab2_us.png)

![Europe Throughput](./transport/lab2/transport_lab2_europe.png)

![australia Throughput](./transport/lab2/transport_lab2_australia.png)

**Explain the relative differences (or lack thereof) of the results.**

Internet traffice is bursty, so the bandwidth fluxuates.  The bandwidth decreases when the links are more
congested with others' packets.  All the regions were bursty.  There weren't detectables differences in 
throughput between them.  It was suprising just how bursty they were.  Throughput fluctated between six and
1 megabits per second.  One time it went as low as 2 kilobits per second.  

# HTTP

## Lab 1: HTTP headers, DevTools

1. List the initial authenticate response header.  
www-Authenticagte: Basic realm="Authentication required"   
 
![request and response headers when no authentication](./http/lab1/devTools_noAuth.png)
**Request and response Headers from dev tools without authorization**  

2. List the subsequent authorization request header.    
*Authorization: Basic bmF0YXMwOm5hdGFzMA==*

![request and response headers with authentication](./http/lab1/devTools_withAuth.png)  
**Above: Request and response Headers from dev tools with authorization**  

### Wire Shark Packet Captures  

![home ip address](./http/homeIP.png)  
**IP Address shown above shows that wireshark captures came from my machine**  

![wire shark request without authorization](./http/lab1/wireshark_noAuth_request.png)
**request without authorization**  

![wire shark response without authorization](./http/lab1/wireshark_noAuth_response.png)
**response without authorization**

![wire shark request with authorization](./http/lab1/wireshark_withAuth_request.png)
**request with authorization**

![wire shark request with authorization](./http/lab1/wireshark_withAuth_response.png)
**response with authorization**  

## Lab 2: Quick/http2
![youtube http](./http/lab2/youTube_http.png)  

![you ](./http/lab2/youtube_netlog.png)  

![you ](./http/lab2/youtube_quic.png)  
**Shows google is using quic**  

![you ](./http/lab2/youTube_wireShark_capture.png)  
**Wire shark capture shows quic is being used because udp is being used rather than tcp**

![you ](./http/lab2/vimdeo_http.png)  
![you ](./http/lab2/vimdeo_quic.png)  
**vimeo is using http/tcp**

# DNS 

## Lab 1: Basics

![local dns server for client resolver](./dns/lab1/cat_etc_resolv.png)
**local DNS server**

![dig with no arguments](./dns/lab1/dig_with_no_args.png)
**Root DNS Servers**

![A record of www.pdx.edu](./dns/lab1/www.pdx.edu_A_record.png)
**A record of www.pdx.edu**

![MX record of www.pdx.edu](./dns/lab1/pdx.edu_MX_record.png)
**MX record of pdx.edu**

1. What do these records explain about how PSU's web/mail services are run?
The domain name for the MX record of pdx.edu all end in google.com.  This is because google rather than psu is hosting the mail service for PSU.  The Email still ends in .pdx.edu becasue mail service aliasing is being used 

![NS record for mashimaro](./dns/lab1/mashimaro_NS.png)
**NS record for mashimaro.cs.pdx.edu**

![A record for mashimaro](./dns/lab1/mashimaro_A_record_from_NS.png)
**A record for mashimaro.cs.pdx.edu**

![NS record for www.thefengs.com](./dns/lab1/thefengs_NS.png)
**Authoritative Server for thefengs.com**

![A record for www.thefengs.com](./dns/lab1/thefengs_A.png)
**A record for thefengs.com**

2. when a web request hits port 80 ojf 131.252.220.66, how does the server know which site to server from?  (i.e. what protocol header)
All http requests are recieved on port 80.  So the fact that port 80 is used specifies that it is request for the web page.  


## lab 2: Iterative Lookup

![f root server ip](./dns/lab2/iterative_lookup/froot_ip.png)
**dig to find IP f.root-server.net**

![f root server query](./dns/lab2/iterative_lookup/froot_server_query.png)
**Query of f.root-server.net**

![second server ip](./dns/lab2/iterative_lookup/2_server_IP.png)
**dig to find IP address of second server**

![second server query](./dns/lab2/iterative_lookup/2_server_query.png)
**Query of second server**

![third server ip](./dns/lab2/iterative_lookup/3_server_ip.png)
**dig to find IP address of third server**

![third server query](./dns/lab2/iterative_lookup/3_server_query.png)
**Query of third server**

1. The third server, ns1.umass.edu, contains the A record for gaia.cs.umass.edu.  Therefore ns1.umass.edu is the authoritative server for gaia.cs.umass.edu.

## Aliases and reverse lookups
```
X=$(dig linuxlab.cs.pdx.edu | egrep "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | gawk '{print $5}')

for i in \`echo $X\`
do 
    #echo $i
    dig -x $i
done
```
![screen shot of reverse look up script](./dns/lab2/reverse_lookup.png)
**Above: Screen shot of output of reverse look up script**

## lab 3: Names of hosts on a subnet
```
X=$(dig -x 131.252.220.{0..255} | egrep "SOA" | gawk '{print $5}')
#echo $X

for i in `echo $X`
do 
    echo $i >> 220hosts.txt
    #dig -x $i
done
```
![screen shot of lab 3 results](./dns/lab3/lab3.png)

# Network Recap

- use ifconfig to find the IP address of the VM and the name of the
local virtual ethernet interface
- Use netstat to find the IP address of the default router

![my ip, default router](./network_recap/ip_and_default_router.png)

- Temporarily change the default DNS server.  Perform a reverse DNS lookup on the DNS server to find its name

![name of DNS](./network_recap/name_of_dns.png)

- Within Wireshark , annotate the packets in the trace to explain the purpose of each of the packets being exchanged

![wire shark capture from oregonctf](./network_recap/wireshark.png)

1. How many DNS requests are made? 
*Two, there is one DNS request for the A record to obtain the IPv4 IP address of the website, and there is a second request to obtain the IPv6 IP address of the website.*

2. How many TCP connections does the browser initiate simultaneously to the site?
*There is only one 3 way handshake show in the packet capture, therefor only one TCP connection is established*

3. How many HTTP GET requests are there for embedded objects?
*There are no embeded objects because the web page only contains the the text hello world.  If there were pictures, there would be addional get requests for each picture*


## Lab 1: Full trace analysis 

# CDN

## Lab 1: Geographic DNS
1. Lookup geographic locations of the following DNS servers  
*PSU DNS server, Portland, OR lat/long: 45.5213, -122.6859*  
*Virginia Tech Server, Blacksburg, VA lat/long: 37.2557, -80.4315*  
- Then, using dig, resolve www.google.com from each of the DNS servers and record each result.  

![psu resolve](./cdn/lab1/psu_resolve.png)
**Above: Resolve of www.google.com from PSU Server**

![Virginia Tech resolve](./cdn/lab1/VTech_resolve.png)
**Above: Resolve of www.google.com from Virginia Tech Server.  The region is District of Columbia (not show)**

2. Lookup up geographic locations for each that do not give Google's Mountain View headquarters as the result?
*PSU's name server was given a IP Address for google located in the Netherlands.*  
*Virginia Tech's name server was given an IP address for google located in Washington D.C.*  

3. What is the geographic distance between the DNS server and web server?  
*Portland to Netherlands = 8038 km*
*Virginia Tech to Washington D.C. = 1533 km*

4. Do the routes reveal any information on the accuracy of the geographic locations given?  
*No, it just gives a bunch of starts after the first couple of routers.*  

![trace route from portland](./cdn/lab1/traceroute.png)
**Above:  Traceroute does not give much information.  Both look the same.**  

## Lab 2: HTTP Load Balancing

- Show the machines running within Cloud Shell and Compute Engine
UI.  

![machines running as seen in cloud sheel](./cdn/lab2/machines_cloud_shell.png)

![machines running as seen in cloud sheel](./cdn/lab2/machines_running_ui.png)

- Show site via IP address assigned to load balancer (the Anycast IPaddress)  

 ![wep page from load balancer](./cdn/lab2/load_balancer_site.png)

1. Which instance group did the request go to?
 *Server Hostname: instance-group-1-nk5l*   
 *Region and Zone: us-east1-b*  

- Show the autoscaling turning on for us-east1 instance group  

![autoscaling turning on](./cdn/lab2/autoscaling.png)

- Peform traceroutes from VM's to pdx.edu

![us east](./cdn/lab2/us_east.png)
**Eastern US**  

![us west](./cdn/lab2/us_west.png)
**Western US**  

![asia](./cdn/lab2/asia.png)
**Asia**  

![Europe](./cdn/lab2/europe_west.png)
**Western Europe**  





