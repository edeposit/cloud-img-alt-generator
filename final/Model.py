class Model():
    def select(self):
        """
        Gets all entries from the datastore
        Each entry contains: publicURL, textDetection, labelDetection, imgTag 
        :return: List of tuples
        """
        pass

    def insert(self, publicURL, textDetection):
        """
        inserts a new accessible photo entry into model
        :param name: String
        :param textDetection: String
        :param labelDetection: String
        :param imgTag: String
        """
        pass
