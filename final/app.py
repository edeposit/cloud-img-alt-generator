"""
A simple photo hosting tea flask app for generating and hosting accessible photos for websites
"""
from flask import Flask, redirect, request, url_for, render_template
import detect
import imgStorage
import os
from werkzeug.utils import secure_filename
from google.cloud import storage


UPLOAD_FOLDER="./fileUploads"
ALLOWED_EXTENSIONS= {'png', 'jpg', 'jpeg', 'gif'}

from model_datastore import model

app = Flask(__name__)       # our Flask app
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
model = model()

"""
Function decorator === app.route('/',index())
"""
@app.route('/')
@app.route('/index.html')
def index():
    """
    accepts requests for root or index.html, and returns landing page that contains stock image  with navbar on top
    """
    return render_template('index.html')

@app.route('/upload', methods=['GET'])
def upload():
    """
    Accepts GET requests and presents form to upload file to model;
    returns add page with fillable form
    """
    return render_template('upload.html')

@app.route('/processimage', methods=['POST'])
def processimage():
    """
    Accepts POST requests to add photo to model;
    uses vision api to detect objects and text in photo and adds them to model
    Redirect to index when completed.
    """
    
    #get image from form
    file = request.files['filePath']

    #if saving to storage bucket
    #storage_client =  storage.Client('expanded-augury-254716')
    storage_client =  storage.Client('deposit-accessible-photos')
    bucket = storage_client.get_bucket('final-project-output')
    #storage_client =  storage.Client(os.environ['PROJECT_ID')
    #bucket = storage_client.get_bucket(os.environ['PHOTO_BUCKET'])

    #get file name
    fileName=imgStorage._safe_filename(file.filename)
    blob = bucket.blob(fileName)

    # write img to bucket 
    blob.upload_from_string(file.read(), content_type='image/jpg')
    blob.make_public()

    # get public url for blob
    url= blob.public_url
    
    #use vision api to get text and what are objects are in photo 
    textDetect= detect.detect_text_uri(url)
    if not textDetect:
        textDetect= ''
        
    labelDetect= detect.detect_labels_uri(url)
    if not labelDetect:
        labelDetect=''

    #create img tage wtih alt attribute
    imgTag= imgStorage.create_img_tag(url, textDetect, labelDetect)

    model.insert(publicURL=url, textDetection=textDetect, labelDetection=labelDetect, imgTag=imgTag) 

    return redirect(url_for('list'))


@app.route('/list', methods=['GET'])
def list():
    """
    Accepts GET requests, and returns page with list of accessible images from from model;
    """
    entries= model.select() 

    #create list of diconaries so values can be accessed by key for each shop
    shopData=[]
    for shop in entries:
        a, b, c, d=shop
        shopDict= {'publicURL':a, 'textDetection':b, 'labelDetection':c, 'imgTag':d }
        shopData.append(shopDict)

    return render_template('list.html', entries=shopData)

# ------------------------------------------------------------------------------------------
#                                     old stuff from homework
# -----------------------------------------------------------------------------------------
# removing this code completely causes build errors

@app.route('/translation', methods=['GET'])
def translation():
    """
    redirects to index
    """
    return redirect(url_for('index'))

@app.route('/add', methods=['GET'])
def add():
    """
    redirects to index
    """
    return redirect(url_for('index'))

@app.route('/new', methods=['POST'])
def new():
    """
    redirects to index
    """
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=8000)

