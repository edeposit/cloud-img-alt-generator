from google.protobuf.json_format import MessageToDict

def detect_text_uri(uri):
    """Detects text in the file located in Google Cloud Storage or on the Web.
        Code addapted from https://github.com/GoogleCloudPlatform/python-docs-samplese
        :param uri: url for photo
        :return: string containing text in photo
    """
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()
    image = vision.types.Image()
    image.source.image_uri = uri

    response = client.text_detection(image=image)

    texts = response.text_annotations

    #unneeded call
    #from_text_detection(texts)
    if texts:
        return from_text_detection(texts)
    else:
        return ''

def from_text_detection(textDetection):
    """
        parse object return from google vision api and converts into string
        of useull text 
        :param textDetection: protobuf object from google cloud vision api
        :return: string containing text in photo
    """
    textList=[]
    msgDict= MessageToDict(textDetection[0])
    #get description and remove new lines
    description= ''
    if 'description' in msgDict:
        description = msgDict['description']
        #print('description =', description )

        splitDescription = description.split('\n')
        for line in splitDescription:
            if line:
                textList.append(line)

    separator= ', '
    textStr=''
    textStr= separator.join(textList)
    return textStr

def detect_labels_uri(uri):
    """
    Detects labels in the file located in Google Cloud Storage or on the Web.
    Code addapted from https://github.com/GoogleCloudPlatform/python-docs-samplese
    :param textDetection: protobuf object from google cloud vision api
    :return: string containing physical objects in photo
    """
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()
    image = vision.types.Image()
    image.source.image_uri = uri

    response = client.label_detection(image=image)
    labels = response.label_annotations

    labelList=[]
    for label in labels:
        labelList.append(label.description)
    
    separator= ', '
    textStr= separator.join(labelList)
    print(textStr)
    return textStr