from werkzeug.utils import secure_filename
import datetime

def _check_extension(filename, allowed_extensions):
    """
    code adapted bookshelf app
    :param fileName: String for file name
    :param allowed_extensions: String list of allowed extensions
    :exception: raiese exception if flie type not allowed
    """
    if ('.' not in filename or
            filename.split('.').pop().lower() not in allowed_extensions):
        raise BadRequest(
            "{0} has an invalid name or extension".format(filename))


def _safe_filename(filename):
    """
    code adapted bookshel app
    generates a safe filename that is unlikely to collide with existing objects
    in google cloud storage.
    ``filename.ext`` is transformed into ``filename-YYYY-MM-DD-HHMMSS.ext``
    """
    filename = secure_filename(filename)
    date = datetime.datetime.utcnow().strftime("%Y-%m-%d-%H%M%S")
    basename, extension = filename.rsplit('.', 1)
    return "{0}-{1}.{2}".format(basename, date, extension)

def create_img_tag(url, textDetection, labelDetection):
    imgTag='<img src="'+ url + '"'
    imgTag= imgTag + ' ' + 'alt=' + '"' + labelDetection + '.' 
    imgTag= imgTag + ' ' + 'Text: ' + textDetection + '"' + '>'
    return imgTag
