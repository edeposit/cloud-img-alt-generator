To run code

virtualenv -p python3 env
source env/bin/activate
export PHOTO_BUCKET="final-project-output"
export PROJECT_ID="expanded-augury-254716"
pip install -r requirements.txt
export GOOGLE_APPLICATION_CREDENTIALS=/Users/evandeposit/expanded-augury-254716-0f054b6bab66.json
python app.py
