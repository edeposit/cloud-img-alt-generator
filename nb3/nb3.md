Evan DePosit  
12/1/2019  
CS 530- Cloud and Internet Systems  
Lab Notebook #3

# Cloud Storage  

## Lab 1  

![quake map](./cloud_storage/lab1/quakeMap.png)


## Lab 2  

- Try the command `gcloud compute instances list`

1. Why did it fail? What role needs to be added to the Service Account's
permissions for the VM to have access to the project's instances?  

*It failed because storage account permissions was set to Storage Object Viewer. So this VM can only view Storage Objects, and compute instances list is not a Storage Object.  The role "Viewer" would give access to read all resources.* 

- Show it successfully copying over, then rename it and then attempt
to copy it back into the bucket

![copy from bucket](./cloud_storage/lab2/copy.png)

![copy to bucket](./cloud_storage/lab2/ls.png)

2. Why did it fail? What role needs to be added to the Service Account's permissions for the VM to have access to add a file to the storage bucket?

*It failed because role has been set to storage object viewer, do the vm does not have permission to edit the contents of the storage bucket, only read what is already there.  If the role had been set to Storage Object Creator, the command would have succeeded.* 

## lab 3

- Visit the URL via a web browser and take a screenshot that includes the entire URL and the image that has been retrieved

![blob public url](./cloud_storage/lab3/blobPublicUrl.png)

# Security 

## Lab 1 

![a1openbucket](./security/a1openbucket.png)

# Compute
## Compute Engine lab 1 

![instance running](./compute/lab1/instanceRunning.png)

![index page](./compute/lab1/index.png)

![add book](./compute/lab1/addBook.png)

## Compute Engine lab 2 

- Show the JSON that is returned
![json returned](./compute/lab2/jsonReturned.png)

- Make the image public (done via GUI previously).  View image in a browser
![public URL](./compute/lab2/publicURL.png)

- Show the screenshot of your image in the storage bucket to include
in your lab notebook
![storage bucket](./compute/lab2/storageBucket.png)

# AWS EC2
## lab 1
![ssh ec2](./aws_ec2/lab1/ssh.png)

# Serverless Databases
## Cloud Datastore Lab 1

![Review added to dataStore](./serverless_databases/lab1/reviewDatastore.png)

# serverless platforms

## App Engine Lab 1

![serverless platforms lab 1](./serverless_platforms/lab1/appEnginelab1.png)

## App Engine Lab 2

![hello world](./serverless_platforms/lab2/helloWorld.png)

## App Engine Lab 3 

![app running from in dev server](./serverless_platforms/lab3/appRunningInDevServer.png)

![app running from app engine](./serverless_platforms/lab3/appRunningFromAppEngine.png)

# Serverless Containers

## Cloud Run Lab 1

- Show the site running using web preview in Cloud Shell

![web preview](./serverless_containers/lab1/webPreview.png)

- Screenshot the container in the WebUI and via the console command

![container from web UI](./serverless_containers/lab1/webUIContainer.png)

![container from console](./serverless_containers/lab1/ConsoleContainer.png)

- Visit the secret URL that gives you proxy access, screenshot the site

![index](./serverless_containers/lab1/helloWorld.png)

![secrete url for proxy axcess](./serverless_containers/lab1/secreteURL.png)

- Screenshot the result of https://google.com being entered as URL

![google.com entered](./serverless_containers/lab1/googleEnteredAsURL.png)

- Screenshot the result of http://169.254.169.254/computeMetadata

![compute metaDATA entered](./serverless_containers/lab1/computeMetadata.png)

1. Is there a directory named v1beta1? 
No, there is one labeled v1.  Entering `http://169.254.169.254/computeMetadata/v1beta1` doesn't lead anywhere because the patched the bug.  

- Attempt to navigate the metadata associated with the VM by accessing paths under http://169.254.169.254/computeMetadata/v1

![navigage meta data](./serverless_containers/lab1/navigateMetadata.png)

2. Read this article and this article to identify the vulnerability that has been fixed.

It was discovered that one could gain access to any container in a subset by exploiting a server side request forgery. The bug was fixed by deploying a metadata concealment proxy, which disabled access to metadata information. 

## Cloud Run lab 2

- Show the output of the deploy command with the URL

![output of deploy](./serverless_containers/lab2/outputOfDeploy.png)

- Visit the site in a browser and screenshot its output that includes the
URL

![output of deploy](./serverless_containers/lab2/visitSite.png)


## Cloud Run lab 3

- Visit the site via the Cloud Shell preview on port 8000; show a screenshot.  

![guest book browser](./serverless_containers/lab3/guestBook.png)

- Take a screenshot of the output of the command and the URL of the
serverless container deployment.   

![container deployment](./serverless_containers/lab3/deploy.png)

- Test application, leave a "Cloud Run Dev" entry into the Guestbook, and show a screenshot.  

![container deployment](./serverless_containers/lab3/cloudRunDeploy.png)

# Serverless Functions

## Cloud Functions Lab 1

![hello world](./cloud_functions/lab1/helloWorldGet.png)

## Cloud Functions Lab 2

![logs](./cloud_functions/lab2/logs.png)

![gore Bucket](./cloud_functions/lab2/goreBucket.png)
**Bucket for cloud function to check for offensive content**

![no gore bucket](./cloud_functions/lab2/noGoreBucket.png)
**Bucket of Blurred images**

photos of lambs and a nude statue were not added to the blurred image bucket by the cloud function

![zombie](./cloud_functions/lab2/zombie.png)

![blurred zombie](./cloud_functions/lab2/blurZombie.png)

## Cloud Functions Lab 3

![log](./cloud_functions/lab3/log.png)

![slack app](./cloud_functions/lab3/kg.png)

# AWS Lambda

## Lab 1

![browser 1](./aws_lambda/lab1/browser1.png)

![browser 2](./aws_lambda/lab1/browser2.png)

![wget time](./aws_lambda/lab1/wgetTime.png)

# IAC Kubernetes Labs

## Kubernetes Lab 1

![tutorial](./iac_kubernetes/lab1/tutorial.png)

## Kubernetes Lab 2

- Visit Compute Engine via the web console and show the following that
have been created
    - Instance templates
    - Instance groups
    - VM instances

![instance templates](./iac_kubernetes/lab2/instanceTemplates.png)

![instance groups](./iac_kubernetes/lab2/instanceGroup.png)

![vm instances](./iac_kubernetes/lab2/vmInstances.png)

- Show output of listing pods with all 3 in a "READY" state

![pods in ready state](./iac_kubernetes/lab2/pods.png)

- Show output of listing services with LoadBalancer indicating an external IP address that is ready

![loadBalancer](./iac_kubernetes/lab2/listingServices.png)

- Visit external IP address of LoadBalancer and show the site running by creating another entry with message "Kubernetes"

![external IP of load balancer](./iac_kubernetes/lab2/kubernetesEntry.png)

- Show app running in Kubernetes Engine via web console

![app running in kubernetes engine](./iac_kubernetes/lab2/kubernetesEngine.png)

- Show load balancer in Network Services and show its details

![load balancer in network serveces](./iac_kubernetes/lab2/loadBalancerNetworkServices.png)

- Show External IP addresses under VPC network to view the ones
created

![external ip address uner vps network](./iac_kubernetes/lab2/externalIPaddresses.png)

- Show Docker image in Container Registry and show its size

![docker image in container registry](./iac_kubernetes/lab2/dockerImage.png)

# AWS Elastic Container Services

## AWS ECS Lab 1

Mistake in ECS Lab #1.... At the end of it, I want you to repeat the lab using your hw3small container from Homework #3.  Will be fixing this and updating the 03_Lab PDF.

- Show the web page served by container via the load balanced IP address/DNS name

![juice web page](./elastic_container_services/lab1/juiceWebPage.png)

- Show the network information of each

![network info 1](./elastic_container_services/lab1/network1.png)

![network info 2](./elastic_container_services/lab1/network2.png)

- Hit each node individually from a browser on port 3000 and show
the output

![hit node 1 from browser](./elastic_container_services/lab1/browser1.png)

![hit node 2 from browser](./elastic_container_services/lab1/browser2.png)

- Then, repeat the process using your hw3small container

- Show the web page served by container via the load balanced IP
address/DNS name

![ip of load balancer for hw3small](./elastic_container_services/lab1/hw3small/bubbleTeaLoadBalancer.png)

- Find the network information of the machines that the two
tasks/containers are being run on from. Show the network information of each

![hw3small task 1 network](./elastic_container_services/lab1/hw3small/task1Network.png)

![hw3small task 2 network](./elastic_container_services/lab1/hw3small/task2Network.png)

- Hit each node individually from a browser on port 3000 and show
the output

![hw3small task 1 browser](./elastic_container_services/lab1/hw3small/task1Browser.png)

![hw3small task 2 browser](./elastic_container_services/lab1/hw3small/task2Browser.png)

# Security 2

![a2finance](./security/a2finance.png)
**a2finance**

![a3password](./security/a3password.png)
**a3password**

![a4error](./security/a4error.png)
**a4error**

![a5power](./security/a5power.png)
**a5power**
